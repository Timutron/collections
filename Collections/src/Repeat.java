import java.util.HashMap;
import java.util.Map;

public class Repeat {
    public static void count(String[] word) {
        Map<String, Long> mp = new HashMap<>();
        long count = 0;
        for (int i = 0; i < word.length; i++) {
            count = 0;
            for (int j = 0; j < word.length; j++) {
                if (word[i].equals(word[j]))
                    count++;
            }
            mp.put(word[i], count);
        }

        System.out.println(mp);
    }
}
