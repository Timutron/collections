import java.util.Arrays;
import java.util.Comparator;

public class TextSorted {
    public static void sort(String[] word){
        Arrays.sort(word, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.println(Arrays.toString(word));
    }
}

